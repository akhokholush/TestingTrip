﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RZD
{

    public static class UrlsPage
    {
        public static string baseUrl = "https://yandex.ru/";
        public static string RaspUrl = "https://rasp.yandex.ru/";
    }
    public static class currDriver
    {
        public static IWebDriver LaunchDriver()
        {
            string fileName = @"../../../chromedriver_win32";
            string fullPath = Path.GetFullPath(fileName);
            IWebDriver webDriver = new ChromeDriver(fullPath);
            return webDriver;
        }
    }


}


