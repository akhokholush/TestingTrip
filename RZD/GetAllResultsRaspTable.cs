﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RZD
{

    public class ResultsRaspTable
    {
        public void GetResultsRaspTable(IWebDriver driver)
        {
            int j = 0;
            string patternRUB = @"\b(\d+\W?Р)";
            string patternPrice = @"\d+";
            string priceString;
            Regex regex = new Regex(patternRUB);

            DateTime parsedReqTime = DateTime.ParseExact(ValuesForFinding.reqTime, "HH:mm", null);

            //Сохранить информацию о транспорте из таблицы результатов
            var SearchSpansTransp = driver.FindElements(By.CssSelector("span[class='SegmentTitle__title']"));
            

            var SearchSpansTime = driver.FindElements(By.CssSelector("div[class='SearchSegment__dateTime Time_important']"));

            //Сохранить цену из таблицы результатов
            var SearchSpansPrice = driver.FindElements(By.CssSelector("div[class='SearchSegment__scheduleAndPrices SearchSegment__scheduleAndPrices_fullWidth']"));

      
            string SpansTimeText = "";
            for (int i = 0; i < SearchSpansTime.Count - 1; i++)
            {
                 Match match = regex.Match(SearchSpansPrice[i].Text);
                 priceString = match.ToString();
                 regex = new Regex(patternPrice);
                 match = regex.Match(priceString);
                 priceString = match.ToString();
                if (SearchSpansTime[i].Text.Length > 6)
                {
                    SpansTimeText = SearchSpansTime[i].Text.Remove(0, SearchSpansTime[i].Text.Length - 5);
                }
                else
                {
                    SpansTimeText = SearchSpansTime[i].Text;
                }
                 if ((DateTime.ParseExact(SpansTimeText, "HH:mm", null).CompareTo(parsedReqTime) == 1))
                 {
                     if (Int32.Parse(priceString) < ValuesForFinding.reqPriceTrip)
                     {
                         Console.WriteLine("Ваш рейс:" + SearchSpansPrice[i].Text + "руб, " + Int32.Parse(priceString)/Values.dollar + " время:"+SearchSpansTime[i].Text);
                         SearchSpansTransp[i].Click();
                         HeadingRoute.AssertHeadingRoute(driver, Values.headingBestWay, By.CssSelector("h1[class='b-page-title__title']"));
                        j = 1;
                         break;
                     }

                 } 
            }
            if (j != 1)
            {
                Console.WriteLine("Не удалось найти желаемый рейс");
            }

        }
        
    }
}
