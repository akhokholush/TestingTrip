﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RZD
{
    public static class HeadingRoute
    {
        public static void AssertHeadingRoute(IWebDriver driver, string text, By by)
        {
            TimeTrip reqData = new TimeTrip();
            reqData.AccountDays = DateTime.Now;
            DateTime accountDays = reqData.AccountDays;
            if (IsElementExists.CatchNoElntExct(driver, by)) // By.CssSelector("header[class='SearchTitle']")))
            {
                IWebElement HeadingRoute = driver.FindElement(by); //By.CssSelector("header[class='SearchTitle']"));//do if exists
                Assert.IsTrue(HeadingRoute.Text.Contains(text), "Заголовок таблицы не соответствует запросу");
                Console.WriteLine(HeadingRoute.Text + " Заголовок таблицы поиска");
            }
            else
            {
                Console.WriteLine("Не удалось найти заголовок поиска");
            }
        }
    }
}
