﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RZD
{
    public static class RouteForm
    {
        public static void FillRouteForm(IWebDriver driver)
        {
            IWebElement FieldFromCity = driver.FindElement(By.Name("fromName"));
            FieldFromCity.Clear();
            FieldFromCity.SendKeys(Values.fromCity);
           
            IWebElement FieldToCity = driver.FindElement(By.Name("toName"));
            FieldToCity.Clear();
            FieldToCity.SendKeys(Values.toCity);
           

            TimeTrip reqData = new TimeTrip();
            reqData.AccountDays = DateTime.Now;
            DateTime accountDays = reqData.AccountDays;
            IWebElement FieldDate = driver.FindElement(By.CssSelector("input[class='date-input_search__input']"));
            FieldDate.SendKeys(accountDays.ToString("d MMMM yyyy"));
         

            Assert.AreEqual(Values.toCity, FieldToCity.GetAttribute("value"), "В поле toCity  отобразилось значение отличное от введенного");
            Assert.AreEqual(Values.fromCity, FieldFromCity.GetAttribute("value"), "В поле fromCity  отобразилось значение отличное от введенного");
            Assert.AreEqual(accountDays.ToString("d MMMM yyyy"), FieldDate.GetAttribute("value"),  "В поле When  отобразилось значение отличное от введенного");

            IWebElement BtnSearch = driver.FindElement(By.CssSelector("button[class='y-button_islet-rasp-search _pin-left _init']"));
            BtnSearch.Click();
        }

    }
}
